
import os
import flask
import jinja2

from search import app
from flask import abort
from flask import request, render_template, redirect, url_for, g, jsonify


def render(filename, context):
    """Generate HTML from data in the context."""

    dir = os.path.dirname(os.path.realpath(__file__))
    loader = jinja2.FileSystemLoader(dir+'/templates')
    env = jinja2.Environment(loader=loader)
    template = env.get_template(filename)
    d = {"d": context}
    return template.render(d)


@app.route('/search', methods=['POST'])
def search():
    """ ."""

    print(request.json)
    return jsonify({'removed': True}), 200


@app.route('/')
def index():
    """Render the default page."""
    return render_template('index.html')

