

import pymongo
import os.path

from search import db_host, db_port


class LuceneQuery(object):
    """ ."""

    def __init__(self, text, engine):
        self.text = None
        self.engine = engine

    def execute(self):
        return True        

    def results(self):
        return True


class MongoQuery(object):
    """ ."""
    # https://api.mongodb.com/python/current/
    
    def __init__(self, text, engine):
        self.text = None
        self.engine = engine

    def execute(self):
        client = pymongo.MongoClient(db_host, db_port)
        return True        

    def results(self):
        return True