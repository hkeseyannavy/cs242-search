
.PHONY: venv run clean

venv:
	pip3 install -r requirements.txt && \
	virtualenv -p python3 venv 

run:
	source ./venv/bin/activate; \
	APP_SETTINGS=config.py FLASK_APP=run.py flask run; \

clean:
	rm -rf venv
	rm -rf __pycache__
	find . -name *.pyc -exec rm -rf '{}' \;

